from random import random


class Player:
    id = 1

    def __init__(self, name):
        """
        :param name: Имя игрока
        :param room: id Комнаты, куда он помещен
        """
        self.name = name
        self.room = None
        self.x = int(random() * 4)
        self.y = int(random() * 4)

        self.id = Player.id
        Player.id += 1

    def addto(self, room):
        self.room = room
        while not self.room.isfree(self.x, self.y):
            self.x = int(random() * 4)
            self.y = int(random() * 4)

    def move_up(self):
        if self.y > 0 and self.room.isfree(self.x, self.y-1):
            self.y -= 1
        self.room.refresh()

    def move_down(self):
        if self.y < 4 and self.room.isfree(self.x, self.y+1):
            self.y += 1
        self.room.refresh()

    def move_right(self):
        if self.x < 4 and self.room.isfree(self.x+1, self.y):
            self.x += 1
        self.room.refresh()

    def move_left(self):
        if self.x > 0 and self.room.isfree(self.x-1, self.y):
            self.x -= 1
        self.room.refresh()
