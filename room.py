from .exceptions import *


class Room:
    id = 0

    def __init__(self):
        self.id = Room.id
        self.players = []
        self.map = [[0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0]]
        Room.id += 1

    def getplayers(self):
        return self.players

    def addplayer(self, player):
        if len(self.players) < 2:
            self.players.append(player)
            self.map[player.x][player.y] = player.id
        else:
            raise RoomIsFullException

    def refresh(self):
        self.map = [[0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0]]
        for player in self.players:
            self.map[player.y][player.x] = player.id

    def render(self):
        s = ''
        for y in self.map:
            for x in y:
                s += str(x)
            s += '\n'

        s = s.replace('0', u'\U000025FB')

        for i in range(1, 2 * Room.id, 2):
            s = s.replace(str(i), u'\U0001F60F')
            s = s.replace(str(i+1), u'\U0001F609')
        return s

    def isfree(self, x, y):
        if self.map[y][x] <= 0:
            return True
        return False

    def arenear(self):
        if len(self.players) == 2:
            distance = (self.players[0].x - self.players[1].x)**2 + (self.players[0].y - self.players[1].y)**2
            if distance <= 2:
                return True
        return False

    def isFull(self):
        if len(self.players) < 2:
            return False
        return True
