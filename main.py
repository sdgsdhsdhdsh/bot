import telebot
from core.controller import RoomManager
from core.player import Player

from telebot import types
markup = types.ReplyKeyboardMarkup()
markup.row('/up')
markup.row('/left', '/down', '/right')

token = '324171914:AAEyA-Z6H7fG7Gmp8_taxSCU4ukZkrsBUm4'

bot = telebot.TeleBot(token)

vasyan = RoomManager()
players = []
channels = {}


def find_plyer(name):
    global players
    for player in players:
        if player.name == name:
            return player


@bot.message_handler(content_types=["text"])
def botmain(message):
    if str(message.text) == '/help':
        bot.send_message(message.chat.id, "/addme для входа в комнату на 2х. Выхода НЕТ")
    if str(message.text) == '/addme':
        if message.chat.username not in [player.name for player in players]:
            new_player = Player(message.chat.username)
            players.append(new_player)
            print("new player ", new_player.name)
            hisroom = vasyan.addplayer(new_player)
            bot.send_message(message.chat.id, "Васян закинул тебя в комнату {}".format(hisroom.id))
            bot.send_message(message.chat.id, hisroom.render(), reply_markup=markup)
            channels[new_player] = message.chat.id
        else:
            bot.send_message(message.chat.id, "Я тебя уже регал")
    try:
        if str(message.text) == '/up':
            player = find_plyer(message.chat.username)
            player.move_up()
            playerzz = player.room.getplayers()
            bot.send_message(channels[playerzz[0]], player.room.render(), reply_markup=markup)
            if len(playerzz) == 2:
                bot.send_message(channels[playerzz[1]], player.room.render(), reply_markup=markup)

        if str(message.text) == '/down':
            player = find_plyer(message.chat.username)
            player.move_down()
            playerzz = player.room.getplayers()
            bot.send_message(channels[playerzz[0]], player.room.render(), reply_markup=markup)
            if len(playerzz) == 2:
                bot.send_message(channels[playerzz[1]], player.room.render(), reply_markup=markup)

        if str(message.text) == '/right':
            player = find_plyer(message.chat.username)
            player.move_right()
            playerzz = player.room.getplayers()
            bot.send_message(channels[playerzz[0]], player.room.render(), reply_markup=markup)
            if len(playerzz) == 2:
                bot.send_message(channels[playerzz[1]], player.room.render(), reply_markup=markup)

        if str(message.text) == '/left':
            player = find_plyer(message.chat.username)
            player.move_left()
            playerzz = player.room.getplayers()
            bot.send_message(channels[playerzz[0]], player.room.render(), reply_markup=markup)
            if len(playerzz) == 2:
                bot.send_message(channels[playerzz[1]], player.room.render(), reply_markup=markup)

        if str(message.text).startswith('/chat'):
            player = find_plyer(message.chat.username)
            if player.room.arenear():
                playerzz = player.room.getplayers()
                bot.send_message(channels[playerzz[0]],
                                 "{}: {}".format(player.name, message.text[5:]),
                                 reply_markup=markup)

                bot.send_message(channels[playerzz[1]],
                                 "{}: {}".format(player.name, message.text[5:]),
                                 reply_markup=markup)
            else:
                bot.send_message(message.chat.id, "Тебя никто не слышит(", reply_markup=markup)
    except AttributeError:
        pass


if __name__ == '__main__':
    bot.polling(none_stop=True)
