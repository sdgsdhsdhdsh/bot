from .room import Room


class RoomManager:
    def __init__(self):
        self.rooms = []

    def findroom(self):
        for room in self.rooms:
            if len(room.players) <= 1:
                return room
        new_room = Room()
        self.rooms.append(new_room)
        return new_room

    def addplayer(self, player):
        room = self.findroom()
        room.addplayer(player)
        player.addto(room)
        return room
